<%-- 
    Document   : index
    Created on : May 6, 2020, 8:53:08 PM
    Author     : Matico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dinosaurios</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row mt-3 mb-5 pt-2 pb-2 border-bottom">
                <div class="col">
                    <h1>Dinosaurios API REST</h1>
                </div>
                <div class="col text-right text-muted">
                    <h3>Matías Andrade Castro</h3>
                    <h4>Taller de Aplicaciones Empresariales</h4>
                    <h5>Sección 50</h5>
                </div>
            </div>

            <div class="alert alert-light mb-4" role="alert">
                Esta API recibe consultas y entrega respuestas en formato JSON.
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Método</th>
                        <th scope="col">URL</th>
                        <th scope="col">Acción</th>
                        <th scope="col">Respuesta</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>GET</td>
                        <td>/api/dinosaurios</td>
                        <td>Lista todos los dinosaurios</td>
                        <td>Lista de dinosaurios</td>
                    </tr>
                    <tr>
                        <td>GET</td>
                        <td>/api/dinosaurios/<var>id</var></td>
                        <td>Busca un dinosaurio por id</td>
                        <td>El dinosaurio encontrado</td>
                    </tr>
                    <tr>
                        <td>POST</td>
                        <td>/api/dinosaurios</td>
                        <td>Ingresa un nuevo dinosaurio</td>
                        <td>El dinosaurio ingresado</td>
                    </tr>
                    <tr>
                        <td>PUT</td>
                        <td>/api/dinosaurios</td>
                        <td>Edita un dinosaurio existente</td>
                        <td>El dinosaurio editado</td>
                    </tr>
                    <tr>
                        <td>DELETE</td>
                        <td>/api/dinosaurios/<var>id</var></td>
                        <td>Elimina un dinosaurio por id</td>
                        <td>El dinosaurio eliminado</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
