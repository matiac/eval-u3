/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.dao.DinosaurioDAO;
import root.persistence.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Dinosaurio;

/**
 *
 * @author Matico
 */
@Path("dinosaurios")
public class DinosauriosRest {
    private final DinosaurioDAO dao = new DinosaurioDAO();

    @Context
    HttpServletRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listar() {
        List<Dinosaurio> dinos = dao.findDinosaurioEntities();
        return Response.ok(200).entity(dinos).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar(@PathParam("id") int id) {
        Dinosaurio dino = dao.findDinosaurio(id);
        return Response.ok(200).entity(dino).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Dinosaurio dino) {
        dao.create(dino);
        return Response.ok(200).entity(dino).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editar(Dinosaurio dino){
        try {
            dao.edit(dino);
        } catch (Exception ex) {
            Logger.getLogger(DinosauriosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(dino).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("id") int id){        
        Dinosaurio dino = dao.findDinosaurio(id);
        
        try {
            dao.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(DinosauriosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(dino).build();
    }
}
