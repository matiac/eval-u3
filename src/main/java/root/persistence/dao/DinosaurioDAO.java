/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.persistence.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Dinosaurio;

/**
 *
 * @author Matico
 */
public class DinosaurioDAO implements Serializable {
    public DinosaurioDAO() {}

    public DinosaurioDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("DinosauriosPU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Dinosaurio dinosaurio) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(dinosaurio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Dinosaurio dinosaurio) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            dinosaurio = em.merge(dinosaurio);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = dinosaurio.getDinId();
                if (findDinosaurio(id) == null) {
                    throw new NonexistentEntityException("The dinosaurio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dinosaurio dinosaurio;
            try {
                dinosaurio = em.getReference(Dinosaurio.class, id);
                dinosaurio.getDinId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dinosaurio with id " + id + " no longer exists.", enfe);
            }
            em.remove(dinosaurio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Dinosaurio> findDinosaurioEntities() {
        return findDinosaurioEntities(true, -1, -1);
    }

    public List<Dinosaurio> findDinosaurioEntities(int maxResults, int firstResult) {
        return findDinosaurioEntities(false, maxResults, firstResult);
    }

    private List<Dinosaurio> findDinosaurioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Dinosaurio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Dinosaurio findDinosaurio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Dinosaurio.class, id);
        } finally {
            em.close();
        }
    }

    public int getDinosaurioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Dinosaurio> rt = cq.from(Dinosaurio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
