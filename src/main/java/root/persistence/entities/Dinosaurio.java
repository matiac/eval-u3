/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Matico
 */
@Entity
@Table(name = "din_dinosaurios")
@NamedQueries({
    @NamedQuery(name = "Dinosaurio.findAll", query = "SELECT d FROM Dinosaurio d"),
    @NamedQuery(name = "Dinosaurio.findByDinId", query = "SELECT d FROM Dinosaurio d WHERE d.dinId = :dinId"),
    @NamedQuery(name = "Dinosaurio.findByDinGenero", query = "SELECT d FROM Dinosaurio d WHERE d.dinGenero = :dinGenero"),
    @NamedQuery(name = "Dinosaurio.findByDinClasificacion", query = "SELECT d FROM Dinosaurio d WHERE d.dinClasificacion = :dinClasificacion"),
    @NamedQuery(name = "Dinosaurio.findByDinEdad", query = "SELECT d FROM Dinosaurio d WHERE d.dinEdad = :dinEdad"),
    @NamedQuery(name = "Dinosaurio.findByDinZona", query = "SELECT d FROM Dinosaurio d WHERE d.dinZona = :dinZona")})
public class Dinosaurio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "din_id")
    private Integer dinId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "din_genero")
    private String dinGenero;
    @Size(max = 2147483647)
    @Column(name = "din_clasificacion")
    private String dinClasificacion;
    @Size(max = 2147483647)
    @Column(name = "din_edad")
    private String dinEdad;
    @Size(max = 2147483647)
    @Column(name = "din_zona")
    private String dinZona;

    public Dinosaurio() {
    }

    public Dinosaurio(Integer dinId) {
        this.dinId = dinId;
    }

    public Dinosaurio(Integer dinId, String dinGenero) {
        this.dinId = dinId;
        this.dinGenero = dinGenero;
    }

    public Integer getDinId() {
        return dinId;
    }

    public void setDinId(Integer dinId) {
        this.dinId = dinId;
    }

    public String getDinGenero() {
        return dinGenero;
    }

    public void setDinGenero(String dinGenero) {
        this.dinGenero = dinGenero;
    }

    public String getDinClasificacion() {
        return dinClasificacion;
    }

    public void setDinClasificacion(String dinClasificacion) {
        this.dinClasificacion = dinClasificacion;
    }

    public String getDinEdad() {
        return dinEdad;
    }

    public void setDinEdad(String dinEdad) {
        this.dinEdad = dinEdad;
    }

    public String getDinZona() {
        return dinZona;
    }

    public void setDinZona(String dinZona) {
        this.dinZona = dinZona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dinId != null ? dinId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dinosaurio)) {
            return false;
        }
        Dinosaurio other = (Dinosaurio) object;
        if ((this.dinId == null && other.dinId != null) || (this.dinId != null && !this.dinId.equals(other.dinId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Dinosaurio[ dinId=" + dinId + " ]";
    }
    
}
